import java.util.Random;
import java.util.Scanner;

/**
 * @author Nyman
 */

public class BookingSystem {

	public static void main(String[] args) {
		Scanner user_input = new Scanner(System.in);
		int firstChoice;
		Random randNr = new Random();
		String firstName, lastName;
		System.out.println("Welcome! Would you like to login as (1) customer, or (2) as admin?");
		
		mainLoop: while (true){
		firstChoice = user_input.nextInt();
		System.out.println("Please enter your first name: ");
		firstName = user_input.next();
		System.out.println("Please enter your last name: ");
		lastName = user_input.next();
		if (firstChoice == 1){
			System.out.println("Welcome " + firstName + " " + lastName + ".");
			while (true){
			int customerChoice;
			System.out.println("What would you like to do?");
			System.out.println("(1) Book/search flight" + "\n" + "(2) Search available flights"
							   + "\n" + "(3) Exit.");
			customerChoice = user_input.nextInt();
			if (customerChoice == 1){ 
				int price, chooseFlight;
				String flyFrom, flyTo, flyDate, bookFlight, bookSure;
				Listing.ListAirports();
				System.out.println("Where would you like to fly from? ");
				flyFrom = user_input.next();
				System.out.println("Where are you going? ");
				flyTo = user_input.next();
				System.out.println("What date would you like to fly? ");
				flyDate = user_input.next();
				System.out.println("Result: ");
				price = randNr.nextInt(6000) + 1000;
				Listing.SearchFlight(flyFrom, flyTo, flyDate);
				System.out.println("Would you like to book flight?");
				bookFlight = user_input.next();
				if(bookFlight.equals("yes")){
					System.out.println("Enter ID of the result: ");
					chooseFlight = user_input.nextInt();
					System.out.println("The price for this flight is " + price + " kr. Are you sure?");
					bookSure = user_input.next();
						if (bookSure.equals("yes")){
							System.out.println (Admin.CreateBooking(firstName, lastName, chooseFlight, price));
							System.out.println("Would you like to print the ticket?");
							String printTicket;
							printTicket = user_input.next();
							if (printTicket.equals("yes")){
								Listing.bookingTicket(flyFrom, flyTo, flyDate, lastName, firstName);
							}
						}
						else if (bookSure.equals("no")) {
							System.out.println("Ok. Returning to main menu.");
						}
				}
				else if (bookFlight.equals("no")){
					System.out.println("Ok. Returning to main menu.");
				}
			}
			else if (customerChoice == 2){

			}
			else if (customerChoice == 3){
				System.out.println("Good bye.");
				break mainLoop;
			}
			}
		}
		else if (firstChoice == 2){
			String delDatabase;
			System.out.println("Welcome " + firstName + " " + lastName + ".");
			System.out.println("Would you like to renew the current database? yes/no");
			delDatabase = user_input.next();
			if (delDatabase.equals("yes")){
				System.out.println(Admin.DeleteDatabase());
			}
			else if (delDatabase.equals("no")){
				System.out.println("Old database will be used.");
			}
			
			while(true){
			int adminChoice;
			System.out.println("What would you like to do?");
			System.out.println("(1) Create new airport" + "\n" + "(2) Create airline" + "\n" + "(3) Create flight" + 
							   "\n" + "(4) List total bookings" + "\n" + "(5) List total airports" + "\n" +
							   "(6) List total airlines" + "\n" + "(7) List total flights" + "\n" + "(8) Exit.");
			adminChoice = user_input.nextInt();
			if (adminChoice == 1){								//Create airport
				String airport_name, airport_town, airport_country;
				System.out.println("Enter airport name, 3 capital letters: ");
				airport_name = user_input.next();
				System.out.println("Enter town of airport: ");
				airport_town = user_input.next();
				System.out.println("Enter country of airport: ");
				airport_country = user_input.next();
				System.out.println(Admin.CreateAirport(airport_name, airport_town, airport_country));
			}
			else if (adminChoice == 2){							//Create airline
				String airline_name, airline_country;
				System.out.println("Enter name of airline: ");
				airline_name = user_input.next();
				System.out.println("Enter country of airline operations: ");
				airline_country = user_input.next();
				System.out.println(Admin.CreateAirline(airline_name, airline_country));
			}
			else if (adminChoice == 3){
				String flight_name, departureDate;
				int departure, destination, airline;
				Listing.ListAirports();
				System.out.println("Enter departure ID from above: ");
				departure = user_input.nextInt();
				System.out.println("Enter destination ID from above: ");
				destination = user_input.nextInt();
				if (destination == departure){
					System.out.println("You cant travel from and to the same airport" +
									   "\n" + "Returning to main menu.. (1) for customer (2) for admin.");
					break;
				}
				else {
				Listing.ListAirlines();
				System.out.println("Enter airline-ID from above: ");
				airline = user_input.nextInt();
				System.out.println("Enter date (20xx-xx-xx): ");
				departureDate = user_input.next();
				System.out.println("Enter name of flight (Two letters, three numbers. (xx123): ");
				flight_name = user_input.next();
				System.out.println(Admin.CreateFlight(flight_name, departure, destination, airline, departureDate));
				}
			}
			else if (adminChoice == 4){
				Listing.ListBookings();
			}
			else if (adminChoice == 5){
				Listing.ListAirports();
			}
			else if (adminChoice == 6){
				Listing.ListAirlines();
			}
			else if (adminChoice == 7){
				Listing.ListFlights();
			}
			else if (adminChoice == 8){
				System.out.println("Good bye.");
				break mainLoop;
			}
		}
	}
}
		user_input.close();
	}
}
