import java.sql.*;
import java.util.regex.Pattern;

/**
 * @author Nyman
 */
public class Admin {

	String airportName, airportTown, airlineName, airportCountry;;
	/*
	 * You'll have to change the username/password to your own mysql information
	 * Otherwise it wont connect to mysql
	 */
	static String URL = "jdbc:mysql://localhost:3306";
	static String username = "root";
	static String pass = "pollypolly";
	
	public Admin(String airport_name, String airline_name, String airport_town, String airport_country) {
		airportName = airport_name;
		airlineName = airline_name;
		airportTown = airport_town;
		airportCountry = airport_country;
	}
	public static String DeleteDatabase(){
		try {
			Connection myConn = DriverManager.getConnection(URL, username, pass);
			Statement myStmt = myConn.createStatement();
			myStmt.executeUpdate("DROP SCHEMA IF EXISTS flight_system;");
			myStmt.executeUpdate("CREATE SCHEMA flight_system;");
			myStmt.executeUpdate("CREATE TABLE flight_system.airports_departure (airport_id INT NOT NULL AUTO_INCREMENT,"
											 + "airport_name VARCHAR(3) NULL, airport_town VARCHAR(45) NULL,"
											 + "airport_country VARCHAR(45) NULL,"
											 + " PRIMARY KEY (airport_id),"
											 + " UNIQUE INDEX airport_name_UNIQUE (airport_name ASC));");
			myStmt.executeUpdate("CREATE TABLE flight_system.airports_destination (airport_id INT NOT NULL AUTO_INCREMENT,"
											 + "airport_name VARCHAR(3) NULL, airport_town VARCHAR(45) NULL,"
											 + "airport_country VARCHAR(45) NULL,"
											 + " PRIMARY KEY (airport_id),"
											 + " UNIQUE INDEX airport_name_UNIQUE (airport_name ASC));");
			myStmt.executeUpdate("CREATE TABLE flight_system.airlines (airline_id INT NOT NULL AUTO_INCREMENT,"
											 + "airline_name VARCHAR(45) NOT NULL,"
											 + " PRIMARY KEY (airline_id, airline_name),"
											 + " UNIQUE INDEX airline_name_UNIQUE (airline_name ASC));");
			myStmt.executeUpdate("CREATE TABLE flight_system.flights (flight_id INT NOT NULL AUTO_INCREMENT,"
											 + " flight_name VARCHAR(5) NULL, flight_departure INT(11) NOT NULL,"
											 + " flight_destination INT(11) NULL, flight_airline INT(11) NULL, "
											 + "departure_date VARCHAR(45) NULL, PRIMARY KEY (flight_id, flight_departure),"
											 + " UNIQUE INDEX flight_name_UNIQUE (flight_name ASC), UNIQUE INDEX departure_date_UNIQUE"
											 + " (departure_date ASC), INDEX FK_flights_departure_idx (flight_departure ASC),"
											 + " INDEX FK_flights_destination_idx (flight_destination ASC),"
											 + " INDEX FK_fligts_airline_idx (flight_airline ASC),"
											 + " CONSTRAINT FK_flights_departure FOREIGN KEY (flight_departure)"
											 + " REFERENCES flight_system.airports_departure (airport_id)"
											 + " ON DELETE NO ACTION ON UPDATE NO ACTION, CONSTRAINT FK_flights_destination"
											 + " FOREIGN KEY (flight_destination) REFERENCES flight_system.airports_destination"
											 + " (airport_id) ON DELETE NO ACTION ON UPDATE NO ACTION, CONSTRAINT FK_fligts_airline"
											 + " FOREIGN KEY (flight_airline) REFERENCES flight_system.airlines (airline_id)"
											 + " ON DELETE NO ACTION ON UPDATE NO ACTION);");
			myStmt.executeUpdate("CREATE TABLE flight_system.bookings (booking_id INT NOT NULL AUTO_INCREMENT,"
											 + " booking_firstname VARCHAR(45) NULL, booking_lastname VARCHAR (45) NULL,"
											 + " booking_flight INT(11) NULL, booking_price INT(11) NULL, PRIMARY KEY (booking_id), CONSTRAINT FK_booking_flight"
											 + " FOREIGN KEY (booking_flight) REFERENCES flight_system.flights"
											 + " (flight_id) ON DELETE NO ACTION ON UPDATE NO ACTION);");
		}
		catch (Exception exc){
			exc.printStackTrace();
		}
		return "Database deleted.";
	}
	public static String CreateAirport(String airport_name, String airport_town, String airport_country){
		
		Pattern AIRPORT_PATTERN = Pattern.compile("^[A-Z]{3}$");
		if (AIRPORT_PATTERN.matcher(airport_name).matches()) {
			try {
				Connection myConn = DriverManager.getConnection(URL, username, pass);
				Statement myStmt = myConn.createStatement();
				myStmt.executeUpdate("INSERT INTO flight_system.airports_departure (airport_name, airport_town, airport_country)"
						+ " VALUES('" + airport_name + "', '" + airport_town + "', '" + airport_country +"');");
				myStmt.executeUpdate("INSERT INTO flight_system.airports_destination (airport_name, airport_town, airport_country)"
						+ " VALUES('" + airport_name + "', '" + airport_town + "', '" + airport_country +"');");
			}
			catch (Exception exc){
				exc.printStackTrace();
			}
		}
		else {
			return "Invalid entry! Only exact 3 characters please.";
		}
		return "Airport named " + airport_name + " created.";
	}
	public static String CreateAirline(String airline_name, String airline_country){
		
		Pattern AIRLINE_PATTERN = Pattern.compile("^[a-zA-Z0-9]{6,}$"); 
		if (AIRLINE_PATTERN.matcher(airline_name).matches()) {
			try {
				Connection myConn = DriverManager.getConnection(URL, username, pass);
				Statement myStmt = myConn.createStatement();
				myStmt.executeUpdate("INSERT INTO flight_system.airlines (airline_name) VALUES('" 
													+ airline_name + "');");
//				myStmt.executeUpdate("INSERT INTO flight_system.airlines (airline_country) VALUES('" 
//													+ airline_country + "');");
			}
			catch (Exception exc){
				exc.printStackTrace();
			}
		}
		else {
			return "Invalid entry! Minimum of 6 characters please.";
		}
		return "Airline named " + airline_name + " created.";
	}
	public static String CreateFlight(String flight_name, int departure, int destination, int airline, String departureDate){

		Pattern FLIGHT_PATTERN = Pattern.compile("^[a-zA-Z]{2}[0-9]{3}$"); 
		if (FLIGHT_PATTERN.matcher(flight_name).matches()) {
			try {
				Connection myConn = DriverManager.getConnection(URL, username, pass);
				Statement myStmt = myConn.createStatement();
				myStmt.executeUpdate("INSERT INTO flight_system.flights (flight_name, flight_departure, flight_destination,"
											+ " flight_airline, departure_date) VALUES('" + flight_name + "', '"
											+ departure + "', '" + destination + "', '" + airline + "', '" + departureDate +"');");
			}
			catch (Exception exc){
				exc.printStackTrace();
			}
		}
		else {
			return "Invalid entry! Flight must be named with 2 letters and 3 numbers (xx123.)";
		}
		
		return "Flight named " + flight_name + " created.";
	}
	public static String CreateBooking (String firstName, String lastName, int chooseFlight, int price){
			try {
				Connection myConn = DriverManager.getConnection(URL, username, pass);
				Statement myStmt = myConn.createStatement();
				myStmt.executeUpdate("INSERT INTO flight_system.bookings (booking_firstname, booking_lastname, booking_flight, booking_price)"
						+ " VALUES('" + firstName + "', '" + lastName + "', '" + chooseFlight  + "', '" + price + "');");
			}
			catch (Exception exc){
				exc.printStackTrace();
			}
		return "Flight booked";
	}
}