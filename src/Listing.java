import java.sql.*;

/**
 * @author Nyman
 */

public class Listing {

	public Listing() {
		
}
	public static void ListAirports(){
		try{
			Connection myConn = DriverManager.getConnection(Admin.URL, Admin.username, Admin.pass);
			Statement myStmt = myConn.createStatement();
			ResultSet result = myStmt.executeQuery("select * from flight_system.airports_departure");
			while (result.next()){
			System.out.println("---("+ result.getString("airport_id") + ") "+ result.getString("airport_town")
					+ " (" + result.getString("airport_name") + ")---");
			}
		}
		catch (Exception exc){
			exc.printStackTrace();
		}
	}
	public static void ListAirlines(){
		try{
			Connection myConn = DriverManager.getConnection(Admin.URL, Admin.username, Admin.pass);
			Statement myStmt = myConn.createStatement();
			ResultSet result = myStmt.executeQuery("select * from flight_system.airlines");
			while (result.next()){
			System.out.println("---(" + result.getString("airline_id") + ") " + result.getString("airline_name") + "---");
			}
		}
		catch (Exception exc){
			exc.printStackTrace();
		}
	}
	public static void ListFlights(){
		try{
			Connection myConn = DriverManager.getConnection(Admin.URL, Admin.username, Admin.pass);
			Statement myStmt = myConn.createStatement();
			ResultSet result = myStmt.executeQuery("select * from flight_system.flights LEFT JOIN flight_system.airports_departure"
													+ " ON flight_departure = airport_id LEFT JOIN flight_system.airports_destination"
													+ " ON flight_destination = flight_system.airports_destination.airport_id"
													+ " LEFT JOIN flight_system.airlines ON flight_airline = airline_id");
			while (result.next()){
			System.out.println("---(" + result.getString("flight_id") + ") " + result.getString("flight_name") + " Departing from: "
						+ result.getString("airport_town") + " (" + result.getString("airport_name") + ") - "
					    + "Destination: " + result.getString("airports_destination.airport_town")
						+ " (" + result.getString("airports_destination.airport_name") + ")" + " Flying with "
					    + result.getString("airline_name") + "---" );
			}
		}
		catch (Exception exc){
			exc.printStackTrace();
		}
	}
	public static void ListBookings(){
		try{
			Connection myConn = DriverManager.getConnection(Admin.URL, Admin.username, Admin.pass);
			Statement myStmt = myConn.createStatement();
			ResultSet result = myStmt.executeQuery("select * from flight_system.bookings LEFT JOIN flight_system.flights"
													+ " ON flight_id = booking_id");
			while (result.next()){
			System.out.println("Flight: " + result.getString("flight_name") + " Passenger: " + result.getString("booking_firstname")
									+ " " + result.getString("booking_lastname") + " " + "Price:" + result.getString("booking_price"));
			}
		}
		catch (Exception exc){
			exc.printStackTrace();
		}
	}
	public static void SearchFlight(String flyFrom, String flyTo, String flyDate){

		try{
			Connection myConn = DriverManager.getConnection(Admin.URL, Admin.username, Admin.pass);
			Statement myStmt = myConn.createStatement();
			ResultSet result = myStmt.executeQuery("select * from flight_system.flights WHERE flight_departure"
													+ " = '" + flyFrom + "' AND flight_destination = '" + flyTo
													+ "' AND departure_date = '" + flyDate + "';");
			while (result.next()){
			System.out.println("---(" + result.getString("flight_id") + ") " + result.getString("flight_name") + "---");
			}
		}
		catch (Exception exc){
			exc.printStackTrace();
		}
		}
	
    public static void bookingTicket(String flyFrom, String flyTo, String flyDate, String lastName, String firstName) {
    	
    	try{
			Connection myConn = DriverManager.getConnection(Admin.URL, Admin.username, Admin.pass);
			Statement myStmt = myConn.createStatement();
			ResultSet result = myStmt.executeQuery("select * from flight_system.bookings LEFT JOIN flight_system.flights"
					+ " ON flight_id = booking_id LEFT JOIN flight_system.airlines ON"
					+ " flight_airline = airline_id LEFT JOIN flight_system.airports_departure"
					+ " ON flight_departure = airport_id LEFT JOIN flight_system.airports_destination"
					+ " ON flight_destination = flight_system.airports_destination.airport_id WHERE booking_firstname"
					+ " = '" + firstName + "' AND booking_lastname = '" + lastName + "';");;
			while (result.next()){
			System.out.println("____________________________________________________________________");
			System.out.println("Date: " + result.getString("departure_date") + "\n" + "Flight: " + result.getString("flight_name")
					+ "\n" + "Flying with: " + result.getString("airline_name")+ "\n" + "Passenger: "
					+ result.getString("booking_firstname") + " " + result.getString("booking_lastname")
					+ "\n" + "Paying: " + result.getString("booking_price") + " SEK" + "\n" + "Flying from "
					+ result.getString("airport_town") + " (" + result.getString("airport_name") + ")" + " To "
					+ result.getString("airports_destination.airport_town")
					+ " (" + result.getString("airports_destination.airport_name") + ")");
            System.out.println("_____________________________________________________________________");
			}
		}
		catch (Exception exc){
			exc.printStackTrace();
		}
    }
}
		
    	
    
